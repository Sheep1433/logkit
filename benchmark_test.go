package logkit

import (
	"testing"
)

// go test -v -bench=. -benchmem -benchtime 30s
func BenchmarkTestLog(b *testing.B) {
	Init()

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		Error("test err")
	}
}

func BenchmarkTestAsyncLog(b *testing.B) {
	Init()

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		Info("test info")
	}
}
