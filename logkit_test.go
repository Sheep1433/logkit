package logkit

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestSetLevelForIssue_6_Case_No_Once(t *testing.T) {
	resetOnce := reentranceInit()
	// initBeforeFix - options will be replace because no sync.Once
	customPath := "custom_log"
	Init(
		Path(customPath),
		MaxSize(10),
	)
	logger := defaultLogger
	resetOnce()
	SetLevel("debug") // option will be reset, like Path, etc
	logger_1 := defaultLogger
	assert.NotEqual(t, logger_1, logger)
	Debug("test debug and will not log to ./custom_log")
	Sync()
	_, err := os.Stat(customPath)
	assert.NotNil(t, err) // will log to ./log instead of ./custom_log
	_, err = os.Stat("./log")
	assert.Nil(t, err)
	assert.Nil(t, os.RemoveAll("./log"))
}

func TestSetLevelForIssue_6_Case_Once(t *testing.T) {
	defer reentranceInit()()

	customLogPath := "custom_log"
	Init(
		Path(customLogPath),
	)
	logger := defaultLogger
	SetLevel("debug") // 覆盖初始化配置
	logger_1 := defaultLogger
	assert.Equal(t, logger_1, logger)

	SetLevel("Info") // 只覆盖日志等级
	logger_2 := defaultLogger
	assert.Equal(t, logger_1, logger_2)

	Info("test info log and will log to ./log")
	Debug("test debug and will not print debug level log")
	Sync()

	_, err := os.Stat(customLogPath + "/info.log")
	assert.Nil(t, err)
	_, err = os.Stat(customLogPath + "/debug.log")
	assert.NotNil(t, err)
	assert.Nil(t, os.RemoveAll(customLogPath))
}

func TestUseShopeeFormat(t *testing.T) {
	defer reentranceInit()()

	Error("test")
	Sync()
	errLogPath := "./log/error.log"
	_, err := os.Stat(errLogPath)
	assert.Nil(t, err)
	fs, err := os.Open(errLogPath)
	assert.Nil(t, err)
	defer fs.Close()

	var blog = make([]byte, 100)
	_, err = fs.Read(blog)
	assert.Nil(t, err)
	logTokens := strings.Split(string(blog), "|")
	assert.NotEqual(t, 0, len(logTokens))
	// 保证以时间戳开头即可被shopee日志搜集工具搜集
	_, err = time.Parse("2006-01-02 15:04:05.000", logTokens[0])
	assert.Nil(t, err)
	// 日志等级信息
	assert.Equal(t, "ERROR", logTokens[1])
	assert.Nil(t, os.RemoveAll("./log"))
}

// 可以多次初始化日志
func reentranceInit() (cancel func()) {
	return func() {
		defaultLogger = nil // reset logger
	}
}

func TestLogkitLogger_Init(t *testing.T) {
	defer Sync()
	Init([]Option{
		Level("DEBUG"),
		EnableConsole(true),
		EnableCaller(true),
	}...)
	Info("info message", zap.String("key", "value"))
	Debug("debug message", zap.String("key", "value"))
	os.RemoveAll("./log")
}

func TestLogkitLogger_SetLevel(t *testing.T) {
	os.RemoveAll("./log")
	Info("info message1", zap.String("key", "value"))
	Debug("debug message1 (not print)", zap.String("key", "value"))
	SetLevel("DEBUG")
	Info("info message2", zap.String("key", "value"))
	Debug("debug message2", zap.String("key", "value"))
	SetLevel("INFO")
	Info("info message3", zap.String("key", "value"))
	Debug("debug message3 (not print)", zap.String("key", "value"))
	Sync()
	Sync()
	infoContent, err := ioutil.ReadFile("./log/info.log")
	assert.Nil(t, err)
	assert.NotEqual(t, strings.Index(string(infoContent), "info message1|key=value"), -1)
	assert.NotEqual(t, strings.Index(string(infoContent), "info message2|key=value"), -1)
	assert.NotEqual(t, strings.Index(string(infoContent), "info message3|key=value"), -1)
	debugContent, err := ioutil.ReadFile("./log/debug.log")
	assert.Nil(t, err)
	assert.NotEqual(t, strings.Index(string(debugContent), "debug message2|key=value"), -1)
	os.RemoveAll("./log")
}

func TestLogkitErrorAsync(t *testing.T) {
	os.RemoveAll("./log")
	defer Sync()
	Init([]Option{
		ErrorAsync(true),
		EnableConsole(true),
		EnableCaller(true),
	}...)
	Info("info message1", zap.String("info_key", "info_value"))
	Error("error message1", zap.String("error_key", "error_value"))
}
