package logkit

import (
	"git.garena.com/shopee/feed/comm_lib/errors"
	"go.uber.org/zap/zapcore"
)

// NewErrorsExtractCore returns core that extracts params from
// error fields with git.garena.com/shopee/feed/comm_lib/errors error types,
// and append them to zap log
func NewErrorsExtractCore(c Core) Core {
	return &errExtraCore{c}
}

type errExtraCore struct {
	zapcore.Core
}

func (c *errExtraCore) With(fields []Field) Core {
	var retFields = make([]zapcore.Field, 0)
	c.extractFields(fields, &retFields)
	fields = append(fields, retFields...)
	return &errExtraCore{
		c.Core.With(fields),
	}
}

func (c *errExtraCore) extractFields(fields []zapcore.Field, retFields *[]zapcore.Field) {
	for _, field := range fields {
		if field.Type != zapcore.ErrorType {
			continue
		}
		err := field.Interface
		for err != nil {
			cause, ok := err.(errors.Causer)
			if !ok {
				break
			}
			*retFields = append(*retFields, cause.Params()...)
			err = cause.Cause()
		}
	}
}
