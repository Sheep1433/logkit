package encoder

import (
	"os"
	"testing"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger *zap.Logger

func TestEncode(t *testing.T) {
	consoleHandler := zapcore.Lock(os.Stdout)
	devEncoder := zapcore.EncoderConfig{
		TimeKey:       "ts",
		LevelKey:      "level",
		NameKey:       "logger",
		CallerKey:     "caller",
		MessageKey:    "msg",
		StacktraceKey: "stacktrace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.LowercaseLevelEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
		},
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	_ = devEncoder
	logger = zap.New(zapcore.NewTee(
		zapcore.NewCore(NewShopeeEncoder(devEncoder), consoleHandler, zapcore.DebugLevel),
	),
		zap.AddCaller(),
		zap.AddCallerSkip(1),
	)

	logger.Info("asddad",
		zap.String("stringKey", "stringKey"),
		zap.Int("intKey", 3),
		zap.Time("start", time.Now()),
		zap.Namespace("objectKey"),
	)
}

func BenchmarkLog(b *testing.B) {
	for i := 0; i < b.N; i++ {
		logger.Info("asddad",
			zap.String("stringKey", "stringKey"),
			zap.Int("intKey", 3),
			zap.Time("start", time.Now()),
			zap.Namespace("objectKey"),
		)
	}
}
