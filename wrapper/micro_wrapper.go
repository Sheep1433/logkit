package wrapper

import (
	"go.uber.org/zap"
)

type MicroWrapper struct {
	log   *zap.Logger
	sugar *zap.SugaredLogger
}

func NewMicroWrapper(log *zap.Logger) *MicroWrapper {
	return &MicroWrapper{log: log, sugar: log.Sugar()}
}

func (l MicroWrapper) Log(args ...interface{}) {
	l.sugar.Info(args...)
}

func (l MicroWrapper) Logf(format string, args ...interface{}) {
	l.sugar.Infof(format, args...)
}
