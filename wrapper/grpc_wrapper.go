package wrapper

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type GRPCWrapper struct {
	log   *zap.Logger
	sugar *zap.SugaredLogger
}

func NewGRPCWrapper(log *zap.Logger) *GRPCWrapper {
	return &GRPCWrapper{log: log, sugar: log.Sugar()}
}

func (l *GRPCWrapper) Debug(args ...interface{}) {}

func (l *GRPCWrapper) Debugf(format string, args ...interface{}) {}

func (l *GRPCWrapper) Info(args ...interface{}) {}

func (l *GRPCWrapper) Infoln(args ...interface{}) {}

func (l *GRPCWrapper) Infof(format string, args ...interface{}) {}

func (l *GRPCWrapper) Warning(args ...interface{}) {}

func (l *GRPCWrapper) Warningln(args ...interface{}) {}

func (l *GRPCWrapper) Warningf(format string, args ...interface{}) {}

func (l *GRPCWrapper) Error(args ...interface{}) { l.sugar.Error(args...) }

func (l *GRPCWrapper) Errorln(args ...interface{}) { l.sugar.Error(args...) }

func (l *GRPCWrapper) Errorf(format string, args ...interface{}) { l.sugar.Errorf(format, args...) }

func (l *GRPCWrapper) Fatal(args ...interface{}) { l.sugar.Fatal(args...) }

func (l *GRPCWrapper) Fatalln(args ...interface{}) { l.sugar.Fatal(args...) }

func (l *GRPCWrapper) Fatalf(format string, args ...interface{}) { l.sugar.Fatalf(format, args...) }

func (l *GRPCWrapper) Printf(format string, args ...interface{}) {}

func (l *GRPCWrapper) V(v int) bool {
	if v <= 0 {
		return !l.log.Core().Enabled(zapcore.DebugLevel)
	}
	return true
}
