package logkit

import (
	"testing"

	syserr "errors"

	"git.garena.com/shopee/feed/comm_lib/errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest"
)

func TestErrorsCore(t *testing.T) {
	var fields = []zapcore.Field{zap.Uint32("key", 1), zap.String("key3", "value")}

	extraCore := zap.WrapCore(func(c zapcore.Core) zapcore.Core {
		return NewErrorsExtractCore(c)
	})
	logger := zaptest.NewLogger(t, zaptest.WrapOptions(extraCore))
	logger.With(zap.Error(errors.Wrap(syserr.New("sys error"), "append msg", fields...))).Error("xxxxx")
	logger.Sync()
}
