package lumberjack

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"
	"testing"
	"time"

	"github.com/BurntSushi/toml"
	"gopkg.in/yaml.v2"
)

// !!!NOTE!!!
//
// Running these tests in parallel will almost certainly cause sporadic (or even
// regular) failures, because they're all messing with the same global variable
// that controls the logic's mocked time.Now.  So... don't do that.

// Since all the tests uses the time to determine filenames etc, we need to
// control the wall clock as much as possible, which means having a wall clock
// that doesn't change unless we want it to.
var fakeCurrentTime = time.Now()

func fakeTime() time.Time {
	return fakeCurrentTime
}

// todo 新建文件
func TestNewFile(t *testing.T) {
	currentTime = fakeTime

	dir := makeTempDir("TestNewFile", t)
	defer os.RemoveAll(dir)
	defaultBufferSize = 3
	l := &Logger{
		Filename: logFile(dir),
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)
	existsWithContent(logFile(dir), b, t)
	fileCount(dir, 1, t)
}

// todo 写入已存在的
func TestOpenExisting(t *testing.T) {
	currentTime = fakeTime
	dir := makeTempDir("TestOpenExisting", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	data := []byte("foo!")
	err := ioutil.WriteFile(filename, data, 0644)
	isNil(err, t)
	existsWithContent(filename, data, t)

	l := &Logger{
		Filename: filename,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	isNil(err, t)
	equals(len(b), n, t)

	// make sure the file got appended
	existsWithContent(filename, append(data, b...), t)

	// make sure no other files were created
	fileCount(dir, 1, t)
}

// todo 写入长文本
func TestWriteTooLong(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat
	dir := makeTempDir("TestWriteTooLong", t)
	defer os.RemoveAll(dir)
	l := &Logger{
		Filename: logFile(dir),
		MaxSize:  5,
	}
	defer l.Close()
	b := []byte("booooooooooooooo!")
	n, err := l.Write(b)
	notNil(err, t)
	equals(0, n, t)
	equals(err.Error(),
		fmt.Sprintf("write length %d exceeds maximum file size %d", len(b), l.MaxSize), t)
	_, err = os.Stat(logFile(dir))
	assert(os.IsNotExist(err), t, "File exists, but should not have been created")
}

// todo
func TestMakeLogDir(t *testing.T) {
	currentTime = fakeTime
	dir := time.Now().Format("TestMakeLogDir" + backupTimeFormat)
	dir = filepath.Join(os.TempDir(), dir)
	defer os.RemoveAll(dir)
	filename := logFile(dir)
	l := &Logger{
		Filename: filename,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)
	existsWithContent(logFile(dir), b, t)
	fileCount(dir, 1, t)
}

// todo
func TestDefaultFilename(t *testing.T) {
	currentTime = fakeTime
	dir := os.TempDir()
	filename := filepath.Join(dir, filepath.Base(os.Args[0])+"-lumberjack.log")
	defer os.Remove(filename)
	l := &Logger{}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)
	existsWithContent(filename, b, t)
}

// 自动滚动
func TestAutoRotate(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat
	dir := makeTempDir("TestAutoRotate", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Filename: filename,
		MaxSize:  10,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	existsWithContent(filename, b, t)
	fileCount(dir, 1, t)

	newFakeTime()

	b2 := []byte("foooooo!")
	n, err = l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)

	// the old logfile should be moved aside and the main logfile should have
	// only the last write in it.
	existsWithContent(filename, b2, t)
	// the backup file will use the current fake time and have the old contents.
	os_Stat = fakeFS.Stat
	info, err := os_Stat(filename)
	isNil(err, t)
	existsWithContent(backupfilename(filename, info.ModTime(), false), b, t)

	fileCount(dir, 2, t)
}

// 第一次写入需要滚动
func TestFirstWriteRotate(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat
	dir := makeTempDir("TestFirstWriteRotate", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Filename: filename,
		MaxSize:  10,
	}
	defer l.Close()

	start := []byte("boooooo!")
	err := ioutil.WriteFile(filename, start, 0600)
	isNil(err, t)

	// this would make us rotate
	b := []byte("fooooooo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	// existsWithContent(filename, b, t)
	os_Stat = fakeFS.Stat
	info, err := os_Stat(filename)
	isNil(err, t)
	existsWithContent(backupfilename(filename, info.ModTime(), false), start, t)

	fileCount(dir, 2, t)

	// first write after a long time(current_time > file.ModTime + roll_period)
	newFakeTime()
	fakeFS = newFakeFS(48 * time.Hour)

	// this would make us rotate
	b1 := []byte("fooo!")
	os_Stat = fakeFS.Stat
	rollerAtBefore := l.rolloverAt
	n, err = l.Write(b1)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b1), n, t)

	// existsWithContent(filename, b, t)
	info, err = os_Stat(filename)
	isNil(err, t)
	existsWithContent(backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false), b, t)

	fileCount(dir, 3, t)
}

// 最多n个日志，更多的会被压缩
func TestMaxBackups(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	os_Stat = newFakeFS().Stat
	dir := makeTempDir("TestMaxBackups", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Filename:   filename,
		MaxSize:    10,
		MaxBackups: 1,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	existsWithContent(filename, b, t)
	fileCount(dir, 1, t)

	newFakeTime()

	// this will put us over the max
	os_Stat = newFakeFS(1 * time.Second).Stat
	b2 := []byte("foooooo!")
	n, err = l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)

	// this will use the new fake time
	info, err := os_Stat(filename)
	isNil(err, t)
	secondFilename := backupfilename(filename, info.ModTime(), false)
	existsWithContent(secondFilename, b, t)

	// make sure the old file still exists with the same content.
	existsWithContent(filename, b2, t)

	fileCount(dir, 2, t)

	newFakeTime()
	// this will make us rotate again
	os_Stat = newFakeFS(5 * time.Second).Stat
	b3 := []byte("baaaaaar!")
	n, err = l.Write(b3)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b3), n, t)

	// this will use the new fake time
	info, err = os_Stat(filename)
	isNil(err, t)
	thirdFilename := backupfilename(filename, info.ModTime(), false)
	existsWithContent(thirdFilename, b2, t)

	existsWithContent(filename, b3, t)
	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(time.Millisecond * 10)

	// should only have two files in the dir still
	fileCount(dir, 2, t)

	// second file name should still exist
	existsWithContent(thirdFilename, b2, t)

	// should have deleted the first backup
	notExist(secondFilename, t)

	// now test that we don't delete directories or non-logfile files
	newFakeTime()

	// create a file that is close to but different from the logfile name.
	// It shouldn't get caught by our deletion filters.
	notlogfile := logFile(dir) + ".foo"
	err = ioutil.WriteFile(notlogfile, []byte("data"), 0644)
	isNil(err, t)

	// Make a directory that exactly matches our log file filters... it still
	// shouldn't get caught by the deletion filter since it's a directory.
	os_Stat = newFakeFS(10 * time.Second).Stat
	info, err = os_Stat(filename)
	isNil(err, t)
	notlogfiledir := backupfilename(filename, info.ModTime(), false)
	err = os.Mkdir(notlogfiledir, 0700)
	isNil(err, t)
	newFakeTime()

	// this will use the new fake time
	os_Stat = newFakeFS(100 * time.Second).Stat
	info, err = os_Stat(filename)
	isNil(err, t)
	fourthFilename := backupfilename(filename, info.ModTime(), false)

	// Create a log file that is/was being compressed - this should
	// not be counted since both the compressed and the uncompressed
	// log files still exist.
	compLogFile := fourthFilename + compressSuffix
	err = ioutil.WriteFile(compLogFile, []byte("compress"), 0644)
	isNil(err, t)
	fourthFilename = backupfilename(filename, info.ModTime(), false)

	// this will make us rotate again
	b4 := []byte("baaaaaaz!")
	n, err = l.Write(b4)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b4), n, t)

	existsWithContent(fourthFilename, b3, t)
	existsWithContent(fourthFilename+compressSuffix, []byte("compress"), t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(time.Millisecond * 10)

	// We should have four things in the directory now - the 2 log files, the
	// not log file, and the directory
	fileCount(dir, 5, t)

	// third file name should still exist
	existsWithContent(filename, b4, t)

	existsWithContent(fourthFilename, b3, t)

	// should have deleted the first filename
	notExist(thirdFilename, t)

	// the not-a-logfile should still exist
	exists(notlogfile, t)

	// the directory
	exists(notlogfiledir, t)
}

func TestCleanupExistingBackups(t *testing.T) {
	// test that if we start with more backup files than we're supposed to have
	// in total, that extra ones get cleaned up when we rotate.

	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestCleanupExistingBackups", t)
	defer os.RemoveAll(dir)

	// make 3 backup files

	data := []byte("data")
	backup := backupFile(dir)
	err := ioutil.WriteFile(backup, data, 0644)
	isNil(err, t)

	newFakeTime()

	backup = backupFile(dir)
	err = ioutil.WriteFile(backup+compressSuffix, data, 0644)
	isNil(err, t)

	newFakeTime()

	backup = backupFile(dir)
	err = ioutil.WriteFile(backup, data, 0644)
	isNil(err, t)

	// now create a primary log file with some data
	filename := logFile(dir)
	err = ioutil.WriteFile(filename, data, 0644)
	isNil(err, t)

	l := &Logger{
		Filename:   filename,
		MaxSize:    10,
		MaxBackups: 1,
	}
	defer l.Close()

	newFakeTime()

	b2 := []byte("foooooo!")
	n, err := l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(time.Millisecond * 10)

	// now we should only have 2 files left - the primary and one backup
	fileCount(dir, 2, t)
}

func TestMaxAge(t *testing.T) {
	tmp := fakeCurrentTime
	defer func() { fakeCurrentTime = tmp }()
	fakeCurrentTime = time.Now()
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 1 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestMaxAge", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Filename: filename,
		MaxSize:  10,
		MaxAge:   2,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	existsWithContent(filename, b, t)
	fileCount(dir, 1, t)

	// two days later
	newFakeTime()
	fakeFS = newFakeFS(48 * time.Hour)
	os_Stat = fakeFS.Stat
	rollerAtBefore := l.rolloverAt

	b2 := []byte("foooooo!")
	n, err = l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)
	// info, err := os_Stat(filename)
	isNil(err, t)
	existsWithContent(backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false), b, t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(10 * time.Millisecond)

	// We should still have 2 log files, since the most recent backup was just
	// created.
	fileCount(dir, 2, t)

	existsWithContent(filename, b2, t)

	// we should have deleted the old file due to being too old
	existsWithContent(backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false), b, t)

	// two days later
	newFakeTime()
	fakeFS = newFakeFS(48 * 2 * time.Hour)
	os_Stat = fakeFS.Stat
	rollerAtBefore = l.rolloverAt

	b3 := []byte("baaaaar!")
	n, err = l.Write(b3)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b3), n, t)
	// info, err = os_Stat(filename)
	isNil(err, t)
	existsWithContent(backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false), b2, t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(10 * time.Millisecond)

	// We should have 2 log files - the main log file, and the most recent
	// backup.  The earlier backup is past the cutoff and should be gone.
	fileCount(dir, 2, t)

	existsWithContent(filename, b3, t)

	// we should have deleted the old file due to being too old
	existsWithContent(backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false), b2, t)
}

func TestOldLogFiles(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestOldLogFiles", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	data := []byte("data")
	err := ioutil.WriteFile(filename, data, 07)
	isNil(err, t)

	// This gives us a time with the same precision as the time we get from the
	// timestamp in the name.
	t1, err := time.Parse(backupTimeFormat, fakeTime().UTC().Format(backupTimeFormat))
	isNil(err, t)

	backup := backupFile(dir)
	err = ioutil.WriteFile(backup, data, 07)
	isNil(err, t)

	newFakeTime()

	t2, err := time.Parse(backupTimeFormat, fakeTime().UTC().Format(backupTimeFormat))
	isNil(err, t)

	backup2 := backupFile(dir)
	err = ioutil.WriteFile(backup2, data, 07)
	isNil(err, t)

	l := &Logger{Filename: filename}
	files, err := l.oldLogFiles()
	isNil(err, t)
	equals(2, len(files), t)

	// should be sorted by newest file first, which would be t2
	equals(t2, files[0].timestamp, t)
	equals(t1, files[1].timestamp, t)
}

func TestTimeFromName(t *testing.T) {
	l := &Logger{Filename: "/var/log/myfoo/foo.log"}
	prefix, ext := l.prefixAndExt()

	tests := []struct {
		filename string
		want     time.Time
		wantErr  bool
	}{
		{"foo.log.2006-01-02.150405", time.Date(2006, 1, 2, 15, 04, 05, 000000000, time.UTC), false},
		{"foo.log.2006-01-02.150405.gz", time.Date(2006, 1, 2, 15, 04, 05, 000000000, time.UTC), false},
		{"foo.log", time.Time{}, true},
	}

	for _, test := range tests {
		got, err := l.timeFromName(test.filename, prefix, ext)
		equals(got, test.want, t)
		equals(err != nil, test.wantErr, t)
	}
}

func TestLocalTime(t *testing.T) {
	currentTime = time.Now
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestLocalTime", t)
	defer os.RemoveAll(dir)

	l := &Logger{
		Filename:  logFile(dir),
		MaxSize:   10,
		LocalTime: true,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	b2 := []byte("fooooooo!")
	n2, err := l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n2, t)

	existsWithContent(logFile(dir), b2, t)
	existsWithContent(backupFileLocal(dir, currentTime()), b, t)
}

func TestRotate(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat
	dir := makeTempDir("TestRotate", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)

	l := &Logger{
		Filename:   filename,
		MaxBackups: 1,
		MaxSize:    100, // megabytes
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	existsWithContent(filename, b, t)
	fileCount(dir, 1, t)

	newFakeTime()
	fakeFS = newFakeFS(48 * time.Hour)
	os_Stat = fakeFS.Stat
	rollerAtBefore := l.rolloverAt

	err = l.Rotate()
	isNil(err, t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(10 * time.Millisecond)
	isNil(err, t)
	filename2 := backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false)
	existsWithContent(filename2, b, t)
	existsWithContent(filename, []byte{}, t)
	fileCount(dir, 2, t)
	newFakeTime()
	fakeFS = newFakeFS(48 * time.Hour)
	os_Stat = fakeFS.Stat
	rollerAtBefore = l.rolloverAt

	err = l.Rotate()
	isNil(err, t)

	// we need to wait a little bit since the files get deleted on a different
	// goroutine.
	<-time.After(10 * time.Millisecond)

	filename3 := backupfilename(filename, time.Unix(rollerAtBefore-1, 0).UTC(), false)
	existsWithContent(filename3, []byte{}, t)
	existsWithContent(filename, []byte{}, t)
	fileCount(dir, 2, t)

	b2 := []byte("foooooo!")
	n, err = l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)

	// this will use the new fake time
	existsWithContent(filename, b2, t)
}

func TestCompressOnRotate(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestCompressOnRotate", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Compress: true,
		Filename: filename,
		MaxSize:  10,
	}
	defer l.Close()
	b := []byte("boo!")
	n, err := l.Write(b)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b), n, t)

	existsWithContent(filename, b, t)
	fileCount(dir, 1, t)

	newFakeTime()

	err = l.Rotate()
	isNil(err, t)

	// the old logfile should be moved aside and the main logfile should have
	// nothing in it.
	existsWithContent(filename, []byte{}, t)

	// we need to wait a little bit since the files get compressed on a different
	// goroutine.
	<-time.After(300 * time.Millisecond)

	// a compressed version of the log file should now exist and the original
	// should have been removed.
	bc := new(bytes.Buffer)
	gz := gzip.NewWriter(bc)
	_, err = gz.Write(b)
	isNil(err, t)
	err = gz.Close()
	isNil(err, t)
	info, err := os_Stat(filename)
	isNil(err, t)
	bFilename := backupfilename(filename, info.ModTime(), false)
	existsWithContent(bFilename+compressSuffix, bc.Bytes(), t)
	notExist(bFilename, t)

	fileCount(dir, 2, t)
}

func TestCompressOnResume(t *testing.T) {
	currentTime = fakeTime
	megabyte = 1
	defaultBufferSize = 10
	defaultFlushInterval = 10 * time.Millisecond
	fakeFS := newFakeFS()
	os_Stat = fakeFS.Stat

	dir := makeTempDir("TestCompressOnResume", t)
	defer os.RemoveAll(dir)

	filename := logFile(dir)
	l := &Logger{
		Compress: true,
		Filename: filename,
		MaxSize:  10,
	}
	defer l.Close()

	// Create a backup file and empty "compressed" file.
	filename2 := backupFile(dir)
	b := []byte("foo!")
	err := ioutil.WriteFile(filename2, b, 0644)
	isNil(err, t)
	err = ioutil.WriteFile(filename2+compressSuffix, []byte{}, 0644)
	isNil(err, t)

	newFakeTime()

	b2 := []byte("boo!")
	n, err := l.Write(b2)
	<-time.After(2 * defaultFlushInterval) // ensure that it will be written into file
	isNil(err, t)
	equals(len(b2), n, t)
	existsWithContent(filename, b2, t)

	// we need to wait a little bit since the files get compressed on a different
	// goroutine.
	<-time.After(300 * time.Millisecond)

	// The write should have started the compression - a compressed version of
	// the log file should now exist and the original should have been removed.
	bc := new(bytes.Buffer)
	gz := gzip.NewWriter(bc)
	_, err = gz.Write(b)
	isNil(err, t)
	err = gz.Close()
	isNil(err, t)
	existsWithContent(filename2+compressSuffix, bc.Bytes(), t)
	notExist(filename2, t)

	fileCount(dir, 2, t)
}

func TestJson(t *testing.T) {
	data := []byte(`
{
	"filename": "foo",
	"maxsize": 5,
	"maxage": 10,
	"maxbackups": 3,
	"localtime": true,
	"compress": true
}`[1:])

	l := Logger{}
	err := json.Unmarshal(data, &l)
	isNil(err, t)
	equals("foo", l.Filename, t)
	equals(5, l.MaxSize, t)
	equals(10, l.MaxAge, t)
	equals(3, l.MaxBackups, t)
	equals(true, l.LocalTime, t)
	equals(true, l.Compress, t)
}

func TestYaml(t *testing.T) {
	data := []byte(`
filename: foo
maxsize: 5
maxage: 10
maxbackups: 3
localtime: true
compress: true`[1:])

	l := Logger{}
	err := yaml.Unmarshal(data, &l)
	isNil(err, t)
	equals("foo", l.Filename, t)
	equals(5, l.MaxSize, t)
	equals(10, l.MaxAge, t)
	equals(3, l.MaxBackups, t)
	equals(true, l.LocalTime, t)
	equals(true, l.Compress, t)
}

func TestToml(t *testing.T) {
	data := `
filename = "foo"
maxsize = 5
maxage = 10
maxbackups = 3
localtime = true
compress = true`[1:]

	l := Logger{}
	md, err := toml.Decode(data, &l)
	isNil(err, t)
	equals("foo", l.Filename, t)
	equals(5, l.MaxSize, t)
	equals(10, l.MaxAge, t)
	equals(3, l.MaxBackups, t)
	equals(true, l.LocalTime, t)
	equals(true, l.Compress, t)
	equals(0, len(md.Undecoded()), t)
}

// makeTempDir creates a file with a semi-unique name in the OS temp directory.
// It should be based on the name of the test, to keep parallel tests from
// colliding, and must be cleaned up after the test is finished.
func makeTempDir(name string, t testing.TB) string {
	dir := time.Now().Format(name + backupTimeFormat)
	dir = filepath.Join(os.TempDir(), dir)
	isNilUp(os.Mkdir(dir, 0700), t, 1)
	return dir
}

// existsWithContent checks that the given file exists and has the correct content.
func existsWithContent(path string, content []byte, t testing.TB) {
	info, err := os.Stat(path)
	isNilUp(err, t, 1)
	equalsUp(int64(len(content)), info.Size(), t, 1)

	b, err := ioutil.ReadFile(path)
	isNilUp(err, t, 1)
	equalsUp(content, b, t, 1)
}

// logFile returns the log file name in the given directory for the current fake
// time.
func logFile(dir string) string {
	return filepath.Join(dir, "foobar.log")
}

func backupfilename(name string, timestamp time.Time, local bool) string {
	// fmt.Print("fakeTime().UTC().Format(backupTimeFormat)", fakeTime().UTC().Format(backupTimeFormat))
	dir := filepath.Dir(name)
	filename := filepath.Base(name)
	ext := filepath.Ext(filename)
	prefix := filename[:len(filename)-len(ext)]
	if !local {
		timestamp = timestamp.UTC()
	}
	ts := timestamp.Format(backupTimeFormat)
	return filepath.Join(dir, fmt.Sprintf("%s%s.%s", prefix, ext, ts))
}
func backupFile(dir string) string {
	return backupfilename(dir+"/foobar.log", fakeTime(), false)
	// fmt.Print("fakeTime().UTC().Format(backupTimeFormat)", fakeTime().UTC().Format(backupTimeFormat))
	// return filepath.Join(dir, "foobar."+fakeTime().UTC().Format(backupTimeFormat)+".log")
}

func backupFileLocal(dir string, ts time.Time) string {
	return backupfilename(dir+"/foobar.log", ts, true)
}

// logFileLocal returns the log file name in the given directory for the current
// fake time using the local timezone.
func logFileLocal(dir string) string {
	return filepath.Join(dir, fakeTime().Format(backupTimeFormat))
}

// fileCount checks that the number of files in the directory is exp.
func fileCount(dir string, exp int, t testing.TB) {
	files, err := ioutil.ReadDir(dir)
	isNilUp(err, t, 1)
	// Make sure no other files were created.
	equalsUp(exp, len(files), t, 1)
}

// newFakeTime sets the fake "current time" to two days later.
func newFakeTime() {
	fakeCurrentTime = fakeCurrentTime.Add(time.Hour * 24 * 2)
}

func notExist(path string, t testing.TB) {
	_, err := os.Stat(path)
	assertUp(os.IsNotExist(err), t, 1, "expected to get os.IsNotExist, but instead got %v", err)
}

func exists(path string, t testing.TB) {
	_, err := os.Stat(path)
	assertUp(err == nil, t, 1, "expected file to exist, but got error from os.Stat: %v", err)
}

type fakeFile struct {
	uid int
	gid int
}

type mockFileInfo struct {
	os.FileInfo
	mockTime time.Time
}

func (mf *mockFileInfo) ModTime() time.Time {
	return mf.mockTime
}
func (mf *mockFileInfo) fuck() string {
	return "fuck"
}

type fakeFS struct {
	timeTravel    time.Duration
	mockfsInfoMao map[string]os.FileInfo
	files         map[string]fakeFile
}

func newFakeFS(trls ...time.Duration) *fakeFS {
	fs := &fakeFS{
		files:         make(map[string]fakeFile),
		mockfsInfoMao: make(map[string]os.FileInfo),
	}
	for _, t := range trls {
		fs.timeTravel += t
	}

	return fs
}

func (fs *fakeFS) Chown(name string, uid, gid int) error {
	fs.files[name] = fakeFile{uid: uid, gid: gid}
	return nil
}
func (fs *fakeFS) Stat(name string) (os.FileInfo, error) {
	if _, ok := fs.mockfsInfoMao[name]; ok {
		return fs.mockfsInfoMao[name], nil
	}
	info, err := os.Stat(name)
	if err != nil {
		return nil, err
	}
	info = &mockFileInfo{info, time.Now().Add(fs.timeTravel)}
	fs.mockfsInfoMao[name] = info

	stat := info.Sys().(*syscall.Stat_t)
	stat.Uid = 555
	stat.Gid = 666
	return info, nil
}
