package logkit

import (
	"os"
	"time"

	"git.garena.com/shopee/feed/comm_lib/logkit/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func newLogger(opts ...Option) (*LogkitLogger, error) {
	options := newOptions()
	for _, opt := range opts {
		opt(options)
	}

	err := level.UnmarshalText([]byte(options.level))
	if err != nil {
		return nil, err
	}

	infoFileHandler := zapcore.AddSync(&lumberjack.Logger{
		Filename:    options.path + "/info.log",
		MaxSize:     options.maxSize,
		MaxBackups:  options.maxBackups,
		MaxAge:      options.maxAge,
		BufferSize:  options.bufferSize,
		ChannelSize: options.channelSize,
		AsyncWrite:  true,
	})
	errFileHandler := zapcore.AddSync(&lumberjack.Logger{
		Filename:    options.path + "/error.log",
		MaxSize:     options.maxSize,
		MaxBackups:  options.maxBackups,
		MaxAge:      options.maxAge,
		BufferSize:  options.bufferSize,
		ChannelSize: options.channelSize,
		AsyncWrite:  options.errorAsync,
	})
	debugFileHandler := zapcore.AddSync(&lumberjack.Logger{
		Filename:    options.path + "/debug.log",
		MaxSize:     options.maxSize,
		MaxBackups:  options.maxBackups,
		MaxAge:      options.maxAge,
		BufferSize:  options.bufferSize,
		ChannelSize: options.channelSize,
		AsyncWrite:  true,
	})

	timeEncoder := func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
	}

	liveEncoder := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     timeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	devEncoder := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     timeEncoder,
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}
	zapCores := []zapcore.Core{
		zapcore.NewCore(options.encoderBuilder(liveEncoder), errFileHandler, NewLevelEnabler(&level, zapcore.ErrorLevel)),
		zapcore.NewCore(options.encoderBuilder(liveEncoder), infoFileHandler, NewLevelEnabler(&level, zapcore.InfoLevel)),
		zapcore.NewCore(options.encoderBuilder(devEncoder), debugFileHandler, NewLevelEnabler(&level, zapcore.DebugLevel)),
	}

	if options.enableConsole {
		consoleHandler := zapcore.Lock(os.Stdout)
		zapCores = append(zapCores, zapcore.NewCore(zapcore.NewConsoleEncoder(devEncoder), consoleHandler, zapcore.DebugLevel))
	}
	// create options with priority for our opts
	defaultOptions := []zap.Option{}
	if options.enableCaller {
		defaultOptions = append(
			defaultOptions,
			zap.AddCaller(),
			zap.AddCallerSkip(1),
		)
	}

	core := zapcore.NewTee(
		zapCores...,
	)

	logger := &LogkitLogger{
		zap.New(NewErrorsExtractCore(core), defaultOptions...),
	}

	return logger, err
}

type LogkitLogger struct {
	*zap.Logger
}

func (wrapper *LogkitLogger) Fatal(msg string, fields ...Field) {
	wrapper.Logger.Fatal(msg, fields...)
}

func (wrapper *LogkitLogger) Error(msg string, fields ...Field) {
	wrapper.Logger.Error(msg, fields...)
}

func (wrapper *LogkitLogger) Info(msg string, fields ...Field) {
	wrapper.Logger.Info(msg, fields...)
}

func (wrapper *LogkitLogger) Warn(msg string, fields ...Field) {
	wrapper.Logger.Warn(msg, fields...)
}

func (wrapper *LogkitLogger) Debug(msg string, fields ...Field) {
	wrapper.Logger.Debug(msg, fields...)
}

func (wrapper *LogkitLogger) With(fields ...Field) *LogkitLogger {
	return &LogkitLogger{wrapper.Logger.With(fields...)}
}

func (wrapper *LogkitLogger) WithFields(fields map[string]interface{}) *LogkitLogger {
	fieldList := make([]Field, 0, len(fields))
	for field, value := range fields {
		fieldList = append(fieldList, Reflect(field, value))
	}
	return wrapper.With(fieldList...)
}

func (wrapper *LogkitLogger) WithField(key string, value interface{}) *LogkitLogger {
	return wrapper.With(Reflect(key, value))
}

func (wrapper *LogkitLogger) WithError(err error) *LogkitLogger {
	return wrapper.With(Err(err))
}
